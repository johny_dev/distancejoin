import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

case class MBR(xmin: Double, ymin: Double, xmax: Double, ymax: Double)

class Partition extends Serializable {

}

object Partition {

  def binaryPartition(points: RDD[(Int, (Double, Double))], extent: (Double, Double, Double, Double), level: Long)
  : RDD[(Double, Double, Double, Double)] = {

    val numPoints = points.count()
    val halfNumPoints = numPoints / 2
    if (level == 0)
      return points.sparkContext.parallelize(List(extent))

    var result = points.sparkContext.parallelize(List.empty[(Double, Double, Double, Double)])

    if (extent._3 - extent._1 > extent._4 - extent._2) {
      val pointsSorted = points.keyBy(_._2._1).sortByKey(ascending = true).values.cache
      val first = pointsSorted.zipWithIndex().filter(_._2 / halfNumPoints == 0).map(_._1)
      val second = pointsSorted.subtract(first)
      val bound = first.map(_._2._1).max()
      val firstExtent = (extent._1, extent._2, bound, extent._4)
      val secondExtent = (bound, extent._2, extent._3, extent._4)
      result ++= binaryPartition(first, firstExtent, level - 1)
      result ++= binaryPartition(second, secondExtent, level - 1)
    }
    else {
      val pointsSorted = points.keyBy(_._2._2).sortByKey(ascending = true).values.cache()
      val first = pointsSorted.zipWithIndex().filter(_._2 / halfNumPoints == 0).map(_._1)
      val second = pointsSorted.subtract(first)
      val bound = first.map(_._2._2).max()
      val firstExtent = (extent._1, extent._2, extent._3, bound)
      val secondExtent = (extent._1, bound, extent._3, extent._4)
      result ++= binaryPartition(first, firstExtent, level - 1)
      result ++= binaryPartition(second, secondExtent, level - 1)
    }

    result
    //result.cache()
  }

  def binaryPartitionSeq(points: Seq[(Int, (Double, Double))], extent: (Double, Double, Double, Double), level: Long)
  : Seq[(Double, Double, Double, Double)] = {


    val numPoints = points.length
    val halfNumPoints = numPoints / 2
    if (numPoints < 3 || level == 0)
      return Seq(extent)

    var result = Seq.empty[(Double, Double, Double, Double)]

    if (extent._3 - extent._1 > extent._4 - extent._2) {
      val pointsSorted = points.sortBy(_._2._1)
      val (first, second) = pointsSorted.splitAt(halfNumPoints)
      val bound = first.map(_._2._1).max
      val firstExtent = (extent._1, extent._2, bound, extent._4)
      val secondExtent = (bound, extent._2, extent._3, extent._4)
      result ++= binaryPartitionSeq(first, firstExtent, level - 1)
      result ++= binaryPartitionSeq(second, secondExtent, level - 1)
    }
    else {
      val pointsSorted = points.sortBy(_._2._2)
      val (first, second) = pointsSorted.splitAt(halfNumPoints)
      val bound = first.map(_._2._2).max
      val firstExtent = (extent._1, extent._2, extent._3, bound)
      val secondExtent = (extent._1, bound, extent._3, extent._4)
      result ++= binaryPartitionSeq(first, firstExtent, level - 1)
      result ++= binaryPartitionSeq(second, secondExtent, level - 1)
    }
    result
  }


  def apply(sc: SparkContext, sampleData: RDD[(Int, (Double, Double))], level: Long, inParallel: Boolean, sample: Long): Array[(MBR, Long)] = {
    val collected = sampleData.collect()
    val extent = new MBR(collected.minBy(_._2._1)._2._1, collected.minBy(_._2._2)._2._2, collected.maxBy(_._2._1)._2._1, collected.maxBy(_._2._2)._2._2)

    if(inParallel){
      val data = if(sample > 0) {
        sc.parallelize(sampleData.takeSample(false, sample.toInt))
      } else sampleData
      val partitions = binaryPartition(data, (extent.xmin, extent.ymin, extent.xmax, extent.ymax), level)
      partitions.map(x => new MBR(x._1, x._2, x._3, x._4)).zipWithIndex().collect()
    } else {

      val data = if (sample > 0) {
        scala.util.Random.shuffle(collected.toList).take(sample.toInt)
        //sampleData.takeSample(false, sample.toInt).toSeq
      } else collected.toSeq

      val partitions = binaryPartitionSeq(data,
        (extent.xmin, extent.ymin, extent.xmax, extent.ymax), level)

      partitions.map(x => new MBR(x._1, x._2, x._3, x._4)).zipWithIndex.map(x => (x._1, x._2.toLong)).toArray
    }

  }


}