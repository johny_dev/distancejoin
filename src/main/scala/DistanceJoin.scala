import com.typesafe.config.ConfigFactory
import org.apache.log4j
import org.apache.spark.{HashPartitioner, SparkConf, SparkContext}
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.log4j.Logger
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions._

import java.io.File
import Metric.time



object DistanceJoin{
  def main (args : Array[String]) {
    Logger.getLogger("org").setLevel(log4j.Level.ERROR)
    val logger = Logger.getLogger("app")
    println("STAAAAAAAAAAAAAAARTT!!!!!!!!!!!!!!!!!!!")
    val config = ConfigFactory.parseFile(new File("application.properties"))

    val datasetA = config.getString("datasetA")
    val datasetB = config.getString("datasetB")
    val output = config.getString("output")
    val partitions: Int = config.getString("partitions").toInt
    val startPartitions: Int = config.getString("startPartitions").toInt
    val distanceQueryR = config.getString("distanceQueryR").toFloat
    val inParallel = config.getBoolean("inParallel")
    val persistResult = config.getBoolean("persistResult")
    val sample = config.getLong("sample")

    val conf = new SparkConf().setAppName("DistanceJoin")
      //.setMaster("local[12]")
    val sc = new SparkContext(conf)
    val spark = SparkSession.builder().getOrCreate()

    val t1 = System.nanoTime

    //  val rddA = sc.textFile(datasetA, partitions)
    //  val pointsA = rddA.map(line => line.split('|'))
    //    .map(line => {
    //      (line(0).toInt, (line(4).toDouble, line(5).toDouble))
    //    })
    //
    //  val rddB = sc.textFile(datasetB, partitions)
    //  val pointsB = rddB.map(line => line.split('|'))
    //    .map(line => {
    //      (line(0).toInt, (line(3).toDouble, line(4).toDouble))
    //    })

    val rddA = sc.textFile(datasetA, startPartitions)
    val pointsA = rddA.map(line => line.split('|'))
      .map(line => {

        (line(0).toInt, (line(1).toDouble, line(2).toDouble))
      }).cache


    //val pointsA1 = pointsA.map(p => (p._2, p._1)).cache()

    val rddB = sc.textFile(datasetB, startPartitions)
    val pointsB = rddB.map(line => line.split('|'))
      .map(line => {
        (line(0).toInt, (line(1).toDouble, line(2).toDouble))
      }).cache


    val level = (math.log10(partitions) / math.log10(2)).toLong
    //  val maxPointsA =  (pointsA.count() / partitions) + 1
    //  val maxHotelsPoints =  (pointsB.count() / partitions) + 1

    //val pointsAData = pointsA.collect()
    //val pointsBData = pointsB.collect()

    //  val sortedX = pointsA.keyBy(_._1).sortByKey(ascending = true).values.cache()
    //  val sortedY = pointsA1.keyBy(_._1).sortByKey(ascending = true).values.cache()
    //
    //  val minX = sortedX.min()._1
    //  val maxX = sortedX.max()._1
    //
    //  val minY = sortedY.min()._1
    //  val maxY = sortedY.max()._1


    //val extend = new MBR(pointsAData.minBy(_._2._1)._2._1, pointsAData.minBy(_._2._2)._2._2, pointsAData.maxBy(_._2._1)._2._1, pointsAData.maxBy(_._2._2)._2._2)

    //val extend = new MBR(minX, minY, maxX, maxY)

    val durationMinMax = (System.nanoTime - t1) / 1e9d
    println("Find Min Max: " + durationMinMax)

    val t2 = System.nanoTime
    val kdTree = Partition(sc, pointsA, level, inParallel, sample)


    val kdTreeB = kdTree.map((mbr) => {
      val xmin = mbr._1.xmin - distanceQueryR
      val ymin = mbr._1.ymin - distanceQueryR

      val xmax = mbr._1.xmax + distanceQueryR
      val ymax = mbr._1.ymax + distanceQueryR

      val newMbr = new MBR(xmin, ymin, xmax, ymax)
      (newMbr, mbr._2)
    })

    val gridA = sc.broadcast(kdTree)
    val gridB = sc.broadcast(kdTreeB)


    def indexingA(point: (Int, (Double, Double))): (Long, (Int, (Double, Double))) = {
      gridA.value.find((mbr) => {
        ((mbr._1.xmin <= point._2._1) && (point._2._1 <= mbr._1.xmax)) && ((mbr._1.ymin <= point._2._2) && (point._2._2 <= mbr._1.ymax))
      }) match {
        case Some(mbr) => {
          (mbr._2, point)
        }
        case None => {
          throw new RuntimeException("Not covered by grid")
        }
      }
    }


    def indexingB(point: (Int, (Double, Double))): (Long, (Int, (Double, Double)), Array[Long]) = {
      val foundIn: Array[(MBR, Long)] = gridB.value.filter((mbr) => {
        ((mbr._1.xmin <= point._2._1) && (point._2._1 <= mbr._1.xmax)) && ((mbr._1.ymin <= point._2._2) && (point._2._2 <= mbr._1.ymax))
      })

      if (foundIn.isEmpty) return (partitions + 1, point, Array())

      (foundIn(0)._2, point, foundIn.slice(1, foundIn.size).map(_._2))
    }


    val indexedA: RDD[(Long, (Int, Double, Double, String))] = pointsA.map(p => {
      val i = (indexingA(p))

      (i._1, (i._2._1, i._2._2._1, i._2._2._2, "A"))
    })

    val indexedB: RDD[(Long, (Int, Double, Double, String))] = pointsB.map(indexingB).flatMap(p => {
      p._3 match {
        case Array() => List((p._1, (p._2._1, p._2._2._1, p._2._2._2, "B")))
        case a => {
          List((p._1, (p._2._1, p._2._2._1, p._2._2._2, "B"))) ++ a.map(partition => (partition, (p._2._1, p._2._2._1, p._2._2._2, "B"))).toList
        }
      }
    })
//    indexedA.count
//    indexedB.count
    val durationKdTree = (System.nanoTime - t2) / 1e9d
    println("build KDTree: " + durationKdTree)

    val t3 = System.nanoTime()


    val partitioner = new HashPartitioner(partitions)

    //val partitionedA = indexedA.partitionBy(partitioner)
    //val partitionedB = indexedB.partitionBy(partitioner)

    //println(partitioned.glom().collect().foreach(p =>println(p.length)))
    //println(partitioned.glom().collect().foreach(p => p.foreach( p => println(p))))
    val durationRepartition = (System.nanoTime - t3) / 1e9d
    println("Repartition: " + durationRepartition)

    val euclidean = (ax: Double, ay: Double, bx: Double, by: Double) => {
      Metric.euclidean(ax, ay, bx, by)
    }

    val t4 = System.nanoTime()

    val finalADF = spark.createDataFrame(indexedA.map(p => (p._2._1, p._2._2, p._2._3, p._1, p._2._4)))
    finalADF.createOrReplaceTempView("A")

    val finalBDF = spark.createDataFrame(indexedB.map(p => (p._2._1, p._2._2, p._2._3, p._1, p._2._4)))
    finalBDF.createOrReplaceTempView("B")


    val durationDataFrame = (System.nanoTime - t4) / 1e9d
    println("to DataFrame: " + durationDataFrame)

    val t5 = System.nanoTime()
    spark.udf.register("euclidean", euclidean)

    val result = spark.sql("SELECT A._1 IDa, A._2 Xa, A._3 Ya, B._1 IDb, B._2 Xb, B._3 Yb FROM A, B" +
      " WHERE A._4=B._4 AND euclidean(A._2, A._3, B._2, B._3) <=" + distanceQueryR)

    if (persistResult)
      result.write.mode("overwrite").csv(output)
    else println(result.count)
    val durationFinal = (System.nanoTime - t5) / 1e9d
    println("Join: " + durationFinal)

    println("Total: " + (System.nanoTime - t1) / 1e9d)

    println("FINISHED")

    sc.stop()
  }
}
