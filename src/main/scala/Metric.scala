object Metric {
  //def euclidean(a: Point, b: Point)  =  math.sqrt(math.pow((a.X - b.X), 2) + math.pow((a.Y - b.Y), 2))
  //def euclidean(a: (Double, Double, Int), b: (Double, Double, Int))  =  math.sqrt(math.pow((a._1 - b._1), 2) + math.pow((a._2 - b._2), 2))

  def euclidean(ax: Double, ay: Double, bx: Double , by: Double) : Double =  math.sqrt(math.pow((ax - bx), 2) + math.pow((ay - by), 2))

  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) + "ns")
    result
  }

}

